require 'redmine'

Redmine::Plugin.register :redmine_kdeadmin do
  name 'Redmine Administrator Extensions'
  author 'Ben Cooksley'
  description 'Provides several command line rake tasks to help manage redmine'
  version '0.0.1'
  url ''
  author_url ''
end
