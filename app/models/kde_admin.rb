class KdeAdmin
  def self.build_gitolite_config( path )
    # Prepare...
    repository_file = File.open( "#{path}/repositories", "r+" )
    repositories    = Repository.find( :all, :order => :project_id )
    admin_role      = Role.find_by_name("KDE Project Manager")
    readonly_repos  = ["repo-management", "amarok-history", "calligra-history", "emerge-history"]
    
    repositories.each do |repo|
      project     = repo.project
      members     = project.users_by_role
      short_desc  = project.short_description.gsub(/\!.+\!/,"").strip
      repo_path   = repository_path( project.identifier, project.parent )

      repository_file.puts("#{repo_path} = \"#{short_desc}\"")
      repository_file.puts("repo #{repo_path}")
      
      if members.has_key?( admin_role ) && !readonly_repos.include?( project.identifier )
        repo_admins = members[admin_role].collect{ |u| u.login }.join(' ')
        repository_file.puts("    RWCD = #{repo_admins}")
      end

      if( project.parent && !project.parent.parent && project.parent.identifier == "websites" )
        repository_file.puts("    R    = @all")
        repository_file.puts("    RW   = @www")
      elsif( project.parent && !project.parent.parent && project.parent.identifier == "sysadmin" )
        repository_file.puts("    R    = @all")
      elsif( project.parent && !project.parent.parent && project.parent.identifier == "qt5" )
        repository_file.puts("    R    = @all")
      elsif readonly_repos.include?( project.identifier )
        repository_file.puts("    R    = @all")
      else
        repository_file.puts("    RW   [0-9](.+) = @all")
        repository_file.puts("    -    [0-9](.+) = @all")
        repository_file.puts("    RW   KDE/(.+)  = @all")
        repository_file.puts("    -    KDE/(.+)  = @all")
        repository_file.puts("    RWC  = @all")
      end
      
      repository_file.puts("")
    end
  end
  
  def self.add_repo( identifier, parent_path, name, description )
    # Find out who our parent will be....
    parent_tree = parent_path.downcase.split('/')
    @parent = Project.find( parent_tree )
    
    # Is the repo path special cased?
    repo_path = repository_path( identifier, @parent )
        
    # Prepare the project....
    @project = Project.new
    @project.identifier  = identifier
    @project.name        = name
    @project.description = description
    @project.is_public   = "1"
    @project.save
    
    # Stage 2 project setup...
    @project.enabled_module_names = ['repository', 'news']
    @project.move_to_child_of( @parent )
    @project.save
    
    # Setup the repository....
    @repository = Repository.factory('Git')
    @repository.project              = @project
    @repository.url       = "/repositories/#{repo_path}.git/"
    @repository.xmlproject_active = "0"
    
    # Create the repository in the DB....
    if( !@repository.save )
      print "Fatal Error - Repository creation failed\n"
      return
    end
  end
  
  def self.add_repo_admin( repository, admin )
    # Lookup the project and user....
    @project = Project.find( repository.split('/') )
    @role    = Role.find_by_name("KDE Project Manager")
    @admins  = []
    
    if !@project || !@role
        print "Fatal Error - Project or Administrative role not found!\n"
        return
    end

    for username in admin.split(";") do
        @user = User.find_by_login( username )
        if !@user
            print "Warning - User #{username} not found, skipping\n"
            next
        end

        @membership = Member.new(:user => @user, :roles => [@role])
        @project.members << @membership
        @project.save
    end
  end
  
  private
  
  def self.repository_path( identifier, parent )
    # Is the repo path special cased?
    return "websites/#{identifier}" if parent && parent.identifier == "websites" && !parent.parent
    return "sysadmin/#{identifier}" if parent && parent.identifier == "sysadmin" && !parent.parent 
    return "others/#{identifier}" if parent && parent.to_a.to_param =~ /^others/
    return "qt/#{identifier}" if parent && parent.identifier == "qt5" && !parent.parent
    identifier
  end
end
