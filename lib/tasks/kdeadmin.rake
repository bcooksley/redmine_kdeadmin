namespace :kdeadmin do
  desc "Generate the various gitolite configuration files for use on git.kde.org."
  task :build_gitolite_config => :environment do
    path = ENV['path']
    KdeAdmin.build_gitolite_config( path )
  end

  desc "Add a repository to KDE Projects, and perform the initial first setup."
  task :add_repo => :environment do
    identifier  = ENV['identifier']
    parent_path = ENV['parent_path']
    name        = ENV['name']
    description = ENV['description']
    KdeAdmin.add_repo( identifier.downcase, parent_path.downcase, name, description )
  end

  desc "Adds the given username as a repository admin for the given repository."
  task :add_repo_admin => :environment do
    repository  = ENV['repository']
    admin       = ENV['admin']
    KdeAdmin.add_repo_admin( repository.downcase, admin )
  end
end